
"""
 p 20) Find the sum of all the degits in 100! 
"""
import time

def computeFactorial(num):
    if (num == 0) or (num ==1):
        return num
    else:
        return num*computeFactorial(num-1)
        
start = time.time()
print sum(int(x) for x in str(computeFactorial(100)))
end = time.time()
print ("total time: {0}". format(end-start))
