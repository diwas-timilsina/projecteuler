"""
 p13 ) Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.
the file is in txt format
"""

import sys

with open(sys.argv[1]) as f:
    lines = f.readlines()

print str(sum(int(line[:-1]) for line in lines))[:10]
