"""
 p 26) Find the value of d < 1000 for which 1/d contains the longest recurring cycle 
in its decimal fraction part.

Note: 
there are three types or rational numbers
1) 1/10 = 0.1 (rationals that terminate. i.e d=10 is divisible for 2 or 5)
2) 1/7 = 0.(142857) rationals with recurring pattern. i.e d=7, that are coprime with 10
3) 1/6 = 0.1(6) rationals that have recuring pattern after some some digits. i.e d= 6 has factors 
that are both divisible by 2 or 3 and coprime with 10

we only have to look at case 2 and 3 type rationals

10^k = 1(mod)n 
we want to find n with max k

"""
import math

def recurringCycleLength(n):
    if n%2 == 0 or n%5 == 0:
        return 0
        
    i = 1 
    while True:
        if ((10**i)-1)%n == 0: return i 
        i+=1
        

maxLength = -1
num = 0 
for val in range(1,1001):
    length = recurringCycleLength(val)
    if maxLength < length:
        maxLength = length
        num = val

print num
