"""
p 33) The fraction 49/98 is a curious fraction, as an inexperienced mathematician in 
attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, 
is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, 
less than one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, 
find the value of the denominator.

"""
import operator
import functools

def factorizeWithRepeats(n):
    factors = [1]
    f = 2
    while n > 1:
        while n % f == 0:
            factors.append(f)
            n /= f
        f += 1
        if f*f > n:
            if n > 1:
                factors.append(n)
                break
    return factors


def reduceFraction(num,denum):
    numFactors = factorizeWithRepeats(num)
    denumFactors = factorizeWithRepeats(denum)
    
    for i in range(len(denumFactors)-1,-1,-1):
        x = denumFactors[i]
        if x in numFactors:
            numFactors.remove(x)
            denumFactors.remove(x)
            
    
    newNum = functools.reduce(operator.mul,numFactors,1)
    newDenum = functools.reduce(operator.mul,denumFactors,1)

    return newNum,newDenum


lst = []
for i in range(10,99):
    for j in range(i+1,100):
        n,d = reduceFraction(i,j)
        if not i%10 == 0 and not j%10 == 0:
            i_str = str(i)
            j_str = str(j)
            common = [i_str[k] for k in range(len(i_str)) if i_str[k] in j_str]
            if len(common) > 0:
                common = common[0]
                n_str = str(i).translate(None,common)
                d_str = str(j).translate(None,common)
           
                n1, d1 = int("0"+n_str),int("0"+d_str)
                n_int, d_int = reduceFraction(n1, d1)
                
                if n_int == n and d_int == d:
                    lst.append((i,j))
        

nProd = 1
dProd = 1
for i,j in lst:
    nProd *= i
    dProd *= j
    
print reduceFraction(nProd,dProd)[1]




            
            
        
