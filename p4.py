"""
 p 4) A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit 
numbers is 9009 = 91  

find the largest palindrome made from the product of two 3-digit numbers?

"""

# function to check if a number is palindrome
def isPalindrome(n):
    n = str(n)
    if len(n) < 2:
        return True 
    else:
        return (n[0] == n[-1] and isPalindrome(n[1:-1]))

largest = 0
for x in range(100,1000):
    for y in range(100,1000):
        if isPalindrome(x*y) and x*y > largest:
            largest = x*y
print(largest)
