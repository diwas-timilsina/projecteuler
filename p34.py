"""
p 34) 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included

"""

def factorial(n):
    if n < 2:
        return 1
    else:
        return n*factorial(n-1)

sum = 0
for i in range(3,99999):
    i_str = str(i)
    i_int = [int(i_str[k]) for k in range(len(i_str))]
    i_sum = 0
    for x in i_int:
        i_sum += factorial(x)
    
    if i_sum == i :
        sum+= i
        print i 
    
print sum
