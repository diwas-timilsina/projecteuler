"""
p 2) By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
"""

totalSum = 0
secondPrevious = 1 
firstPrevious = 1 

while firstPrevious <= 4000000:
    num = firstPrevious + secondPrevious
    if num % 2 == 0:
        totalSum += num
    secondPrevious = firstPrevious
    firstPrevious = num
print(totalSum)
