"""
 p 11) What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally) in the 20
"""



import sys
import decimal

with open(sys.argv[1]) as f:
    lines = f.readlines()

grid = []
for line in lines:
    grid.append([ int(x) for x in line.split(' ')])

print len(grid)

def mul_list(l):
    result = 1
    for n in l:
        result *= n
    return result

greatest = 0
#Horizontal
for row in grid:
    for i in range(len(row)-3):
        result = mul_list(row[i:i+4])
        if result > greatest:
            greatest = result

#Vertical
for c in range(len(grid[0])):
    col = [grid[h][c] for h in range(len(grid))]
    for i in range(len(col)-3):
        result = mul_list(col[i:i+4])
        if result > greatest:
            greatest = result

#Diagonal right
for r in range(len(grid)-3): 
    for c in range(len(grid)-3):
        diagonal = []
        for i in range(4):
            diagonal.append(grid[r+i][c+i])
        result = mul_list(diagonal)
        if result > greatest:
            greatest = result

#Diagonal left
for r in range(len(grid)-1,2,-1):
    for c in range(len(grid)-3):
        diag = []
        for i in range(4):
            diag.append(grid[r-i][c+i])
        result = mul_list(diag)
        if result > greatest:
            greatest = result

print(greatest)
