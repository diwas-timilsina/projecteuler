"""
p 31)Find the sum of all products whose multiplicand/multiplier/product 
     identity can be written as a 1 through 9 pandigital.

"""

def isPandigit(string,length):
    l = list(set([int(string[i]) for i in range(len(string))]))
    for i in range(1,10):
        if not i in l:
            return False
    return True


result = set()
for i in range(1,9999):
    for j in range(i,9999):
        mul = i*j
        string = str(i)+str(j)+str(mul)
        if len(string)>9: break
        if isPandigit(string,9):
            print (i,j,mul)
            result.add(mul)

print sum(result)
