"""
p 36) Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
"""


def getBinary(num):
    if num < 2:
        return str(num)
    else:
        return getBinary(num/2) + str(num%2)


def isPalindrom(string):
    if len(string)< 2:
        return True
    else:
        return string[0] == string[-1] and isPalindrom(string[1:-1])

  
sum= 0
for i in range (1,1000000):
    binaryString = getBinary(i)
    if isPalindrom(binaryString) and isPalindrom(str(i)):
        sum+=i

print sum
