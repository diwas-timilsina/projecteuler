"""
p 31) In England the currency is made up of pound, and pence, p, and there are eight 
coins in general circulation: 1p, 2p, 5p, 10p, 20p, 50p, 100p, 200p

How many different ways can 2 pounds be made using any number of coins? 
"""
import math

coins = [1,2,5,10,20,50,100,200]
ways = [0]*200
ways.insert(0,1)

for i in range(len(coins)):
    for j in range(coins[i],201):
        ways[j] += ways[j-coins[i]]

print ways[-1]
