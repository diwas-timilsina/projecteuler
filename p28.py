"""
p 28) What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?

"""

def sumDiagonal(size):
    
    total = size*size +1
    
    num = 1 
    sum = 0
    jump = 2
    jumpCount = 0
    while num < total:
        sum += num
        num += jump
        jumpCount = (jumpCount+1)%4
        if jumpCount == 0:
            jump+=2
    return sum

print sumDiagonal(1001)
