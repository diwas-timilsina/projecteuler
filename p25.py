"""
p 25) The 12th term, F12, is the first term in the Fibonacci sequence to contain three digits.
What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
"""
from math import *

i = 0
cnt = 2
limit = 10**999
fib = []
 
fib.append(1)
fib.append(1)
fib.append(2)
 
while (fib[i] <= limit):
    i = (i + 1) % 3;
    cnt+=1;
    fib[i] = fib[(i + 1) % 3] + fib[(i + 2) % 3];

print cnt
