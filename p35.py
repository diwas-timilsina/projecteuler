"""
p 35) How many circular primes are there below one million?
"""
from commonFunctions import generatePrime,is_prime

primeList = generatePrime(1000000)


def generateRotation(num):
    num_str = str(num)
    for i in range(len(num_str)-1):
        num_str = num_str[-1] + num_str[:-1]
        yield int(num_str)
        
circularPrimes = []
for prime in primeList: 
    g = generateRotation(prime)
    addPrime = True
    for i in range(len(str(prime))-1):
        if not is_prime(g.next()):
            addPrime = False
            break
    if addPrime:
        circularPrimes.append(prime)
    
print len(circularPrimes)
