"""
p 24) A permutation is an ordered arrangement of objects. 
For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are 
listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 
0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
"""
import math
import time

start = time.time()
permutationList = []
# generate permuatation
def generatePermutation(prefix,string):
    n = len(string)
    if n == 1 : 
        permutationList.append(prefix+string)
    else:
        for i in range(n):
            generatePermutation(prefix + string[i],string[0:i]+string[i+1:])


#permuation
def permutation(numList):
   generatePermutation("",numList)

permutation("0123456789")
print permutationList[1000000-1]
end = time.time()
print end-start
