"""
p 30) Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
"""

total = 0
for i in range(2,295245):
    if i == sum(int(x)**5 for x in str(i)):
        print i
        total += i

print total
