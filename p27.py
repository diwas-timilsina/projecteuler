"""

p 27) Find the product of the coefficients, a and b, for the quadratic expression 
that produces the maximum number of primes for consecutive values of n, starting with n = 0.

"""
from commonFunctions import is_prime

maxNum = 0
maxA = 0
maxB = 0

for a in range(-999,1000):
    for b in range(-999,1000):
        n = 0
        while is_prime(n**2 + a*n + b):
            n+=1
        if maxNum < n:
            maxNum = n
            maxA = a
            maxB = b

print (maxA*maxB)
