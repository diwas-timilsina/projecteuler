"""
p 7) By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
     What is the 10 001st prime number 

"""


def generatePrime(n):
    """Generate a list of primes of a given size."""
    primes = []
    f = 2
    while len(primes) < n:
        is_prime = True
        for p in primes:
            if f % p == 0:
                is_prime = False
                break
        if is_prime:
            primes.append(f)
        f = 3 if f == 2 else f+2
    return primes

print(generatePrime(10001)[-1])

    
