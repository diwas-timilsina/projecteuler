# This File contains some common functions that 
# will be used to solve other project euler questions 
# 

import math

# compute the gcd of two numbers
def gcd(a,b):
    while b:
        a,b = a,a%b
    return a

# lcm of two numbers
def lcm(a,b):
    return a*b / gcd(a,b)

# generate fibonacci sequence
def getFiboTerm(term):
    if term < 3:
        return 1
    else:
        return getFiboTerm(term-1)+getFiboTerm(term-2)
        
# return the unique factors of a number
def factorize(n):
    factors = []
    f = 2
    while n > 1:
        while n % f == 0:
            factors.append(f)
            n /= f
        f += 1
        if f*f > n:
            if n > 1:
                factors.append(n)
                break
    return list(set(factors))

# factors with possible repeats    
def factorizeWithRepeats(n):
    factors = []
    f = 2
    while n > 1:
        while n % f == 0:
            factors.append(f)
            n /= f
        f += 1
        if f*f > n:
            if n > 1:
                factors.append(n)
                break
    return factors
  

#check if a number is prime 
def is_prime(n):
    if n < 2: 
       return False
    if n % 2 == 0: 
        return n == 2
    f = 3
    while f*f <= n:
        if n % f == 0:
            return False
        f = 3 if f == 2 else f+2
    return True

#genaerate prime numbers
def generatePrime(max=100):
    """Lazily generate the next prime."""
    primes = []
    n = 2
    while (n < max):
        isprime = True
        for p in primes:
            if p*p > n:
                break
            if n % p == 0:
                isprime = False
                break
        if isprime:
            yield n
            primes.append(n)
        n = 3 if n == 2 else n+2


# generate prime factors
def getPrimeFactors(num):
    primes = generatePrime(math.sqrt(num)) 
    factors = []
    for p in primes:
        if num % p == 0:
            factors.append(p)
            while num % p == 0:
                num //= p
    return factors
    

# get proper divisor of a number
def properDivisors(num):
    divisors = [1]
    for i in range(2,int(math.sqrt(num))+1):
        if num % i == 0:
            divisors.append(i)
            if (num/i != i):
                divisors.append(num/i)
    return divisors

if __name__ == "__main__":
    print properDivisors(4)
    print getFiboTerm(40)
